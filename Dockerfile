FROM node

EXPOSE 4000

RUN npm install i npm@latest -g

COPY package.json package-lock*.json ./

RUN npm install
