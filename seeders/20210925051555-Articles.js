'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
   
    await queryInterface.bulkInsert('Articles', [
      {
        id:1,
        author: "ADA LIMÓN",
        title: "BRIGHT DEAD THINGS",
        body: "A book of bravado and introspection, of 21st century feminist swagger and harrowing terror and loss, this fourth collection considers how we build our identities out of place and human contact—tracing in intimate detail",
        createdAt: new Date(),
        updatedAt: new Date(),
      },{
        id:2,
        author: "CHEN CHEN",
        title: "WHEN I GROW UP I WANT TO BE A LIST OF FURTHER POSSIBILITIES",
        body: "In this ferocious and tender debut, Chen Chen investigates inherited forms of love and family—the strained relationship between a mother and son, the cost of necessary goodbyes—all from Asian American, immigrant, and queer perspectives.",
        createdAt: new Date(),
        updatedAt: new Date(),
      },{
        id:3,
        author: "AIMEE NEZHUKUMATATHIL",
        title: "OCEANIC",
        body: "With inquisitive flair, Aimee Nezhukumatathil creates a thorough registry of the earth’s wonderful and terrible magic. In her fourth collection of poetry, she studies forms of love as diverse and abundant as the ocean itself.",
        createdAt: new Date(),
        updatedAt: new Date(),
      }
    ], {});
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('Articles', null, {});
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
