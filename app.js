const express = require('express')
const app = express()
const port = 4000
const articlesController = require('./controller/ArticleController')

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.get('/', (req, res) => {
  res.send('Hello World!')
})

app.post('/articles', articlesController.createArticles)
app.get('/articles', articlesController.getArticles)


app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})


module.exports = app
