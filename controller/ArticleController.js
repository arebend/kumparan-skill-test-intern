const {Article} = require('../models')
const {Op} = require('sequelize')
const {successResponse,errorResponse} = require('../helper/responseFormatter')

async function createArticles(req,res) {
  try {
    const { author, title, body} = req.body
    await Article.create({
      author, title, body
    })
    // return res.json(createArticle)
    successResponse(res, 201, Article)
  } catch (error) {
    // return res.json(error.message)
    errorResponse(res, 401, err.message)
  }
}

async function getArticles(req,res) {
  try {
    const article = await Article.findAll({ 
    order: [['createdAt', 'DESC']],
    where: {
      [Op.and] : [{
        [Op.or] : 
          [{ title:
            { [Op.substring] : req.query.query ? req.query.query : ''}
          },
          { body: 
            { [Op.substring] : req.query.query ? req.query.query: ''}
          }]
        },
        { author : 
          { [Op.substring] : req.query.author?req.query.author:"" } 
        }
      ],
    },  
  })
    // return res.json(article)
  successResponse(res, 200, article)
  } catch (error) {
    // return res.json(error.message)
    errorResponse(res, 422, err.message)
  }
}



module.exports = {createArticles, getArticles}