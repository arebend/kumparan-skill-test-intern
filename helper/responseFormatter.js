exports.successResponse = function (res, code, data) {
  res.status(code).json({
    status: 'success',
    data
  })
}

exports.errorResponse = (res, code, message) => {
  res.status(code).json({
    status: 'fail',
    message: message
  })
}