const request = require('supertest');
const app = require('../app');
const db = require('../models'); 
const { Article } = require('../models')


describe('Test Article API Collection', () => {
  beforeAll(() => {
    return db.sequelize.query(`TRUNCATE "author", "title", "body" RESTART IDENTITY`)
  })

  afterAll(() => {
    return Promise.all([
      db.sequelize.query(`TRUNCATE "author", "title", "body" RESTART IDENTITY`),
      Article.destroy({
        where: { 
          author: 'Ian', 
          title: 'Pecinta Wanita', 
          body: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.'
        },
        truncate: true,
        cascade: true
      })
    ])
  })

  describe('POST /articles', () => {
    test('Success create article. Status code 201', done => {
      request(app)
        .post('/articles')
        .set('Content-Type', 'application/json')
        .send({
          author: 'Eugene', 
          title: 'Pecinta Wanita 2', 
          body: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.'
        })
        .then(res => {
          expect(res.statusCode).toEqual(201);
          expect(res.body.status).toEqual('success');
          done();
        })
    })
    test('Failed create article. Status code 401', done => {
      request(app)
        .post('/articles')
        .set('Content-Type', 'application/json')
        .send({
          author: 'xxx', 
          title: 'xxx', 
          body: 'xxx'
        })
        .then(res => {
          expect(res.statusCode).toEqual(401);
          expect(res.body.status).toEqual('fail');
          done();
        })
    })
  })

  describe('GET /articles', () => {
    test('should successfuly show all articles', done => {
      request(app)
        .get('/articles')
        .set('Content-Type', 'application/json')
        .then((res) => {
          const { body, status } = res
          expect(status).toEqual(200)
          expect(body.status).toEqual('success')
          done()
        })
    })
  })
});
